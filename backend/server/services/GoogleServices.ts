import { google } from 'googleapis';
import { googleConfig } from "../config/Google";

export class GoogleServices {

    public createConnection() {
        return new google.auth.OAuth2(
            googleConfig.clientId,
            googleConfig.clientSecret,
            googleConfig.redirect
        );
    }

    public getConnectionUrl(auth: any, token: String) {
        return auth.generateAuthUrl({
            access_type: 'offline',
            prompt: 'consent',
            state: token,
            scope: 'openid email profile'
            // scope: this._defaultScope
        });
    }

    public getGooglePlusApi(auth) {
        return google.plus({ version: 'v1', auth });
    }

    public getUrl(token: string): string {
        const auth = this.createConnection();
        const url = this.getConnectionUrl(auth, token);
        return url;
    }

    public async getUserData(code: string) {
        const auth = this.createConnection();
        const data = await auth.getToken(code);
        const tokens = data.tokens;
        auth.setCredentials(tokens);
        const plus = this.getGooglePlusApi(auth);
        const me = await plus.people.get({ userId: 'me' });
        const userGoogleId = me.data.id;
        const userGoogleEmail = me.data.emails && me.data.emails.length && me.data.emails[0].value;

        return {
            id: userGoogleId,
            email: userGoogleEmail,
            tokens: tokens,
        };
    }
};

