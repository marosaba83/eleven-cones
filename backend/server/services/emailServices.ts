import { readFile, readFileSync } from "fs";
import { compile, template } from "handlebars";
import { IBasicError } from '../interfaces/basicError';
import { sendgridConfig } from '../config/Sendgrid';
import { setApiKey, send } from "@sendgrid/mail";


export class EmailServices {

    // constructor(
    //     public transporter?: any
    // ) {}

    public async sendMail(
        templateUri: string,
        to: string,
        subject: string,
        replacers: Object
    ) {

        try {
            let htmlFile = readFileSync(__dirname + templateUri, { encoding: 'utf-8' });
            let template = compile(htmlFile);
                        
            const msg = {
                to: to,
                from: 'do-not-reply@elevencones.com',
                subject: subject,
                html: template(replacers)
            };
            
            setApiKey(sendgridConfig.apiKey);
            return send(msg);

        } catch (error) {
            return <IBasicError>{
                code: 'EMAIL_SENDING_ERROR',
                field: 'email',
                message: 'Email sending error.' + error
            };
        }
    };
}
