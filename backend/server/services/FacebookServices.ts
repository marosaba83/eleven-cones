import { facebookConfig } from "../config/Facebook";
import { get } from "request-promise-native";

export class FacebookService {

    private async _getAccessToken(clientId: string, redirectUri: string, clientSecret: string, code: string): Promise<any> {
        let baserUrl: string = 'https://graph.facebook.com/v3.2/oauth/access_token'
        let queryParams: string = '?client_id={clientId}&redirect_uri={redirectUri}&client_secret={clientSecret}&code={code}'
            .replace('{clientId}', clientId)
            .replace('{redirectUri}', redirectUri)
            .replace('{clientSecret}', clientSecret)
            .replace('{code}', code);

        let uri: string = baserUrl + queryParams
        let response: any = {};

        await get(uri)
            .then((body) => {
                let partial = JSON.parse(body);
                response = partial.access_token;
            })
            .catch((err) => {
                response = err.toString();
            });

        return response;
    }

    private async _getFacebookUserId(inputToken: string, accessToken: string) {
        let baserUrl: string = 'https://graph.facebook.com/debug_token'
        let queryParams: string = '?input_token={inputToken}&access_token={accessToken}'
            .replace('{inputToken}', inputToken)
            .replace('{accessToken}', accessToken);

        let uri: string = baserUrl + queryParams
        let response: any = {};

        await get(uri)
            .then((body) => {
                response = JSON.parse(body);
            })
            .catch((err) => {
                response = err.toString();
            });

        return response.data.user_id;
    }

    public async _getFacebookUserData(facebookUserId: string, accessToken: string) {
        let fields = 'id, email, first_name, last_name, name';
        let baseUrl: string = 'https://graph.facebook.com/';
        let queryParams: string = '?fields={fields}&access_token={accessToken}'
            .replace('{fields}', fields)
            .replace('{accessToken}', accessToken);

        let uri: string = baseUrl + facebookUserId + queryParams;
        let response: any = {};

        await get(uri)
            .then((body) => {
                response = JSON.parse(body);
            })
            .catch((err) => {
                response = JSON.parse(err);
            });

        return response;
    }

public async _getFacebookPicture (facebookUserId: string) {
    // https://graph.facebook.com/10217908041189111/picture?height=500

    let baseUrl: string = 'https://graph.facebook.com/{facebookUserId}/picture'
        .replace('{facebookUserId}', facebookUserId);
    let queryParams: string = '?height=400';

    let uri: string = baseUrl + queryParams;
    let response: any = {};

    await get(uri)
        .then((body) => {
            response = JSON.parse(body);
        })
        .catch((err) => {
            response = JSON.parse(err);
        });

    return response;

}


    public getUrl(token: string): string {
        // Retrieve Facebook Auth Url user uses to log in
        let clientId: string = facebookConfig.clientId;
        let redirectUri: string = facebookConfig.redirect;

        let url: string = 'https://www.facebook.com/v3.2/dialog/oauth?client_id={clientId}&redirect_uri={redirectUri}&state={token}&scope={scope}'
            .replace('{clientId}', clientId)
            .replace('{redirectUri}', redirectUri)
            .replace('{token}', token)
            .replace('{scope}', 'email');

        return url;
    }

    public async getUserData(code: string) {
        // Exchanging Code for an Access Token
        let clientId: string = facebookConfig.clientId;
        let redirectUri: string = facebookConfig.redirect;
        let clientSecret: string = facebookConfig.clientSecret;
        let accessToken: string = facebookConfig.accessToken;

        let inputToken: string = await this._getAccessToken(clientId, redirectUri, clientSecret, code);
        let facebookUserId: any = await this._getFacebookUserId(inputToken, accessToken);
        let facebookUserData: any = await this._getFacebookUserData(facebookUserId, accessToken);
        // facebookUserData.picture = await this._getFacebookPicture(facebookUserId);
        facebookUserData.picture = 'https://graph.facebook.com/{facebookUserId}/picture?height=400'.replace('{facebookUserId}', facebookUserId);

        return facebookUserData;
    }

}