import { RedisClient, createClient } from "redis";

export class CacheService {
    private redisClient: RedisClient = createClient();

    public getClient(): RedisClient {
        return this.redisClient;
    }
}