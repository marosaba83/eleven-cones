import { Application } from "express"
import { json, urlencoded } from "body-parser"
import { BaseRoutes } from "../routes/baseRoutes"

export class BaseMiddleware {

    private _app: Application;

    constructor(app: Application) {
        this._app = app;
        this.configuration();
    }

    public configuration() {
        this._app.use(json());
        this._app.use(urlencoded({ extended: false }));

        this._app.use(new BaseRoutes().routes);
    }

}
