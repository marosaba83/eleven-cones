import { BaseRepository } from "./baseRepository";
import { ITournamentDocument } from "../interfaces/tournamentDocument";
import { TournamentSchema } from "../schemas/tournamentSchema"

export class TournamentRepository extends BaseRepository<ITournamentDocument> {
    constructor() {
        super(TournamentSchema);
    };
}
