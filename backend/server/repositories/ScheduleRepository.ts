import { BaseRepository } from "./baseRepository";
import { IScheduleDocument } from "../interfaces/scheduleDocument";
import { ScheduleSchema } from "../schemas/scheduleSchema"

export class ScheduleRepository extends BaseRepository<IScheduleDocument> {
    constructor() {
        super(ScheduleSchema);
    };
}
