import { BaseRepository } from "./baseRepository";
import { IStadiumDocument } from "../interfaces/stadiumDocument";
import { StadiumSchema } from "../schemas/stadiumSchema"

export class StadiumRepository extends BaseRepository<IStadiumDocument> {
    constructor() {
        super(StadiumSchema);
    };
}
