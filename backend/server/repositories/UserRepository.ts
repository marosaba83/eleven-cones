import { BaseRepository } from "./baseRepository";
import { IUserDocument } from "../interfaces/userDocument";
import { UserSchema } from "../schemas/userSchema";
import { hashSync, compare } from 'bcryptjs';

export class UserRepository extends BaseRepository<IUserDocument> {
    constructor() {
        super(UserSchema);
    };

    public async findByEmail(email: String): Promise<IUserDocument> {
        return UserSchema.findOne({
            email: email,
            isActive: true
        });
    }

    public async findByGoogleId(googleId: String): Promise<IUserDocument> {
        return UserSchema.findOne({
            'google.id': googleId,
            'isActive': true
        });
    }

    public async findByFacebookId(facebookId: String): Promise<IUserDocument> {
        return UserSchema.findOne({
            'facebook.id': facebookId,
            'isActive': true
        });
    }

    public async findByValidPasswordToken(token: String): Promise<IUserDocument> {
        return UserSchema.findOne({
            'local.passwordReset.token': token,
            'local.passwordReset.expired': { $gt: new Date() },
            'isActive': true
        });
    }

    public generatePasswordHash(password: string): string {
        const salt: number = 12;
        return hashSync(password, salt);
    }

    public async comparePasswordHash(pasword: string, hashedPassword: string): Promise<boolean> {
        return compare(pasword, hashedPassword);
    }

}
