import { BaseRepository } from "./baseRepository";
import { ITeamDocument } from "../interfaces/teamDocument";
import { TeamSchema } from "../schemas/teamSchema"
import { ObjectId } from 'bson';

export class TeamRepository extends BaseRepository<ITeamDocument> {
    constructor() {
        super(TeamSchema);
    };

    public async join(teamId: String, squad: ObjectId[]): Promise<ITeamDocument> {
        return TeamSchema.findByIdAndUpdate(teamId, { squad: squad }, { new: true });
    }

}
