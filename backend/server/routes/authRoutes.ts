import { Router } from "express";
import { AuthLocalController } from "../controllers/authLocalController";
import { AuthGoogleController } from "../controllers/AuthGoogleController";
import { AuthFacebookController } from "../controllers/AuthFacebookController";

export class AuthRoutes {

    private _authLocalController: AuthLocalController;
    private _authGoogleController: AuthGoogleController;
    private _authFacebookController: AuthFacebookController;

    constructor() {
        this._authLocalController = new AuthLocalController();
        this._authGoogleController = new AuthGoogleController();
        this._authFacebookController = new AuthFacebookController();
    }

    public get routes(): Router {
        let router = Router();

        // LOCAL //
        router.post('/v1/auth/login', (req, res) => this._authLocalController.login(req, res));
        router.post('/v1/auth/signup', (req, res) => this._authLocalController.signup(req, res));
        router.post('/v1/auth/forgot-password', (req, res) => this._authLocalController.forgotPassword(req, res));
        router.get('/v1/auth/reset-password/:token', (req, res) => this._authLocalController.validatePasswordToken(req, res));
        router.post('/v1/auth/reset-password/:token', (req, res) => this._authLocalController.resetPassword(req, res));

        // GOOGLE //
        router.get('/v1/auth/google/link', (req, res) => this._authGoogleController.generateGoogleLink(req, res));
        router.get('/v1/auth/google/continue', (req, res) => this._authGoogleController.continueWithGoogle(req, res));

        // FACEBOOK //
        router.get('/v1/auth/facebook/link', (req, res) => this._authFacebookController.generateFacebookLink(req, res));
        router.get('/v1/auth/facebook/continue', (req, res) => this._authFacebookController.continueWithFacebook(req, res));

        return router;
    }

}
