import { Request, Response } from 'express';
import { RedisClient } from 'redis';
import { sign, decode } from 'jsonwebtoken';
import { jwtConfig } from '../config/jwtConfig';
import { CacheService } from '../services/CacheServices';
import { GoogleServices } from "../services/GoogleServices";
import { UtilServices } from '../services/utilServices';
import { IBasicError } from '../interfaces/basicError';
import { IUserDocument } from '../interfaces/userDocument';
import { IUser } from '../interfaces/user';
import { UserRepository } from '../repositories/userRepository';

export class AuthGoogleController {

    private _cacheServices: CacheService;
    private _utilServices: UtilServices;
    private _googleServices: GoogleServices;
    private _userRepository: UserRepository;

    private _redisClient: RedisClient;

    constructor() {
        this._cacheServices = new CacheService();
        this._utilServices = new UtilServices();
        this._googleServices = new GoogleServices();
        this._userRepository = new UserRepository();

        this._redisClient = this._cacheServices.getClient();
    }

    GOOGLE_TOKEN: string = 'google-auth-token:';

    private _generateGoogleRedisKey(token: string): string {
        return this.GOOGLE_TOKEN + token;
    }

    private _getDecodedtoken(token: string): any {
        return decode(token);
    }

    private _getSignedPayload(payload): string {
        return sign(payload, jwtConfig.secretKey, { expiresIn: jwtConfig.expiresIn });
    }

    public async generateGoogleLink(req: Request, res: Response): Promise<Response> {
        try {
            let token: string = this._utilServices.generateToken();
            let redisKey: string = this._generateGoogleRedisKey(token);
            await this._redisClient.set(redisKey, token);
            await this._redisClient.setex(redisKey, 30, 'expired');
            let googleLink: string = this._googleServices.getUrl(token);

            return res.status(200).json({ 'googleLink': googleLink });

        } catch (error) {
            return res.status(400).json(error);
        }
    }

    public async continueWithGoogle(req: Request, res: Response): Promise<Response> {
        try {
            let newUser: IUserDocument;
            let user: IUser;
            let token: string;
            let googleUser: any;
            let googleUserTokens: any;
            let cacheClient: RedisClient = this._cacheServices.getClient();
            let code: string = req.query.code;
            let googleToken: string = this.GOOGLE_TOKEN + req.query.state;
            let tokenfound = await cacheClient.get(googleToken);
            let googleIdFound: IUserDocument;
            let userId: string;

            if (!tokenfound) {
                throw <IBasicError>{
                    code: 'INVALID_GOOGLE_STATE',
                    field: 'google',
                    message: 'Google state is invalid.'
                };
            }

            googleUser = await this._googleServices.getUserData(code);
            googleUserTokens = await this._getDecodedtoken(googleUser.tokens['id_token']);

            googleIdFound = await this._userRepository.findByGoogleId(googleUser.id);

            // if not exists, create
            if (!googleIdFound) {
                user = {
                    firstName: googleUserTokens.given_name,
                    lastName: googleUserTokens.family_name,
                    email: googleUser.email,
                    pictureUrl: googleUserTokens.picture,
                    google: {
                        id: googleUser.id,
                        token: googleUser.tokens.id_token,
                        name: googleUserTokens.name,
                        username: googleUser.email
                    },
                    isActive: true
                }

                newUser = await this._userRepository.create(<IUserDocument>user);
                userId = newUser._id;
            }

            if (googleIdFound) {
                userId = googleIdFound._id;
            }

            token = this._getSignedPayload({ id: userId });

            return res.status(200).json({ 'token': token });

        } catch (error) {
            return res.status(400).json(error);
        }
    }

}