import { Request, Response } from 'express';
import { RedisClient } from 'redis';
import { sign } from 'jsonwebtoken';
import { jwtConfig } from '../config/jwtConfig';
import { CacheService } from '../services/CacheServices';
import { FacebookService } from "../services/FacebookServices";
import { UtilServices } from '../services/utilServices';
import { IBasicError } from '../interfaces/basicError';
import { IUserDocument } from '../interfaces/userDocument';
import { IUser } from '../interfaces/user';
import { UserRepository } from '../repositories/userRepository';

export class AuthFacebookController {

    private _cacheServices: CacheService;
    private _utilServices: UtilServices;
    private _facebookServices: FacebookService;
    private _userRepository: UserRepository;

    private _redisClient: RedisClient;

    constructor() {
        this._cacheServices = new CacheService();
        this._utilServices = new UtilServices();
        this._facebookServices = new FacebookService();
        this._userRepository = new UserRepository();

        this._redisClient = this._cacheServices.getClient();
    }

    FACEBOOK_TOKEN: string = 'facebook-auth-token:';

    private _generateFacebookRedisKey(token: string): string {
        return this.FACEBOOK_TOKEN + token;
    }

    private _getSignedPayload(payload): string {
        return sign(payload, jwtConfig.secretKey, { expiresIn: jwtConfig.expiresIn });
    }

    public async generateFacebookLink(req: Request, res: Response): Promise<Response> {
        try {
            let token: string = this._utilServices.generateToken();
            let redisKey: string = this._generateFacebookRedisKey(token);
            await this._redisClient.set(redisKey, token);
            await this._redisClient.setex(redisKey, 30, 'expired');
            let facebookLink: string = this._facebookServices.getUrl(token);

            return res.status(200).json({ 'facebookLink': facebookLink });

        } catch (error) {
            return res.status(400).json(error);
        }
    }

    public async continueWithFacebook(req: Request, res: Response): Promise<Response> {
        try {
            let newUser: IUserDocument;
            let user: IUser;
            let token: string;
            // let googleUser: any;
            // let googleUserTokens: any;

            // let code: String = req.query.code;
            // let googleIdFound: IUserDocument;
            let userId: string;

            // -- Check state sent to facebook is valid -- //
            let redisKey: string = this._generateFacebookRedisKey(req.query.state);
            let tokenfound = await this._redisClient.get(redisKey);
            if (!tokenfound) {
                throw <IBasicError>{
                    code: 'INVALID_FACEBOOK_STATE',
                    field: 'facebook',
                    message: 'Facebook state is invalid.'
                };
            }

            // -- If Facebook response is Cancel -- //
            let errors = {
                code: String,
                reason: String,
                description: String
            }

            errors.code = req.body.error;
            errors.reason = req.body.error_reason;
            errors.description = req.body.error_description;

            if (errors.code || errors.reason || errors.description) {
                throw <IBasicError>{
                    code: 'FACEBOOK_CANCEL',
                    field: 'facebook',
                    message: 'User cancels login with Facebook.'
                };
            }


            // -- If Facebook response is Accept -- //
            let code: string = req.query.code;
            let facebookUser: any = await this._facebookServices.getUserData(code);
            let facebookUserFound = await this._userRepository.findByFacebookId(facebookUser.id);

            // if not exists, create
            if (!facebookUserFound) {
                user = {
                    firstName: facebookUser.first_name,
                    lastName: facebookUser.last_name,
                    email: facebookUser.email,
                    pictureUrl: facebookUser.picture,
                    facebook: {
                        id: facebookUser.id,
                        token: code,
                        name: facebookUser.name,
                        username: facebookUser.email
                    },
                    isActive: true
                }

                    newUser = await this._userRepository.create(<IUserDocument>user);
                    userId = newUser._id;
            }

            if (facebookUserFound) {
                userId = facebookUserFound._id;
            }

            token = this._getSignedPayload({ id: userId });

            return res.status(200).json({ 'token': token });

        } catch (error) {
            return res.status(400).json(error);
        }
    }

}