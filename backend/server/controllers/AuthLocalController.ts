import { Request, Response } from 'express';
import { sign } from 'jsonwebtoken';
import { jwtConfig } from '../config/jwtConfig';
import { UserRepository } from '../repositories/userRepository';
import { IUserDocument } from '../interfaces/userDocument';
import { UserController } from './userController';
import { IUser } from '../interfaces/user';
import { IBasicError } from '../interfaces/basicError';
import { UtilServices } from '../services/utilServices';
import { EmailServices } from '../services/EmailServices';
import * as moment from 'moment';

export class AuthLocalController {

    private _userRepository: UserRepository;
    private _utilServices: UtilServices;
    private _emailServices: EmailServices;

    constructor() {
        this._userRepository = new UserRepository();
        this._utilServices = new UtilServices();
        this._emailServices = new EmailServices();
    }

    private _getSignedPayload(payload): String {
        return sign(payload, jwtConfig.secretKey, { expiresIn: jwtConfig.expiresIn });
    }

    public async login(req: Request, res: Response): Promise<Response> {

        const VALIDATION_EMPTY: String = 'VALIDATIONS_EMTPY';
        const VALIDATION_LOGIN: String = 'VALIDATIONS_LOGIN';

        let userFound: IUserDocument;
        let token: String;
        let passwordValid: Boolean;

        try {

            if (!req.body.email) {
                throw <IBasicError>{
                    code: VALIDATION_EMPTY,
                    field: 'email',
                    message: 'Email cannot be empty.'
                };
            }

            if (!req.body.password) {
                throw <IBasicError>{
                    code: VALIDATION_EMPTY,
                    field: 'password',
                    message: 'Password cannot be empty.'
                };
            }

            userFound = await this._userRepository.findOne({ email: req.body.email });

            if (!userFound) {
                throw <IBasicError>{
                    code: VALIDATION_LOGIN,
                    field: 'login',
                    message: 'Email or Password isn\'t correct.'
                };
            }

            passwordValid = await this._userRepository.comparePasswordHash(req.body.password, <string>userFound.local.password);

            if (!passwordValid) {
                throw <IBasicError>{
                    code: VALIDATION_LOGIN,
                    field: 'login',
                    message: 'Email or Password isn\'t correct.'
                };
            }

            token = this._getSignedPayload({ id: userFound._id });

            return res.status(200).json({ 'token': token });

        } catch (error) {
            return res.status(400).json(error);
        }

    }

    public async signup(req: Request, res: Response): Promise<Response> {
        try {
            let emailFound: IUserDocument;
            let token: String;
            let newUser: IUserDocument;
            let user: IUser
            let userEmail: String = req.body.email;

            emailFound = await this._userRepository.findByEmail(userEmail);

            if (emailFound) {
                throw <IBasicError>{
                    code: 'DUPLICATED_EMAIL',
                    field: 'email',
                    message: 'Email already registered.'
                };
            }

            user = {
                email: req.body.email,
                local: {
                    password: this._userRepository.generatePasswordHash(req.body.password)
                },
                isActive: true
            };

            newUser = await UserController._userRepository.create(<IUserDocument>user);

            token = this._getSignedPayload({ id: newUser._id });

            return res.status(200).json({ 'token': token });
        } catch (error) {
            return res.status(400).json(error);
        }

    }

    public async forgotPassword(req: Request, res: Response): Promise<Response> {
        try {
            let user: IUserDocument;
            let updatedUser: IUserDocument;
            let replacers: Object;
            let userEmail: String = req.body.email;
            let token: String = this._utilServices.generateToken();
            let resetPasswordUrl: string = 'https://localhost:3000/v1/auth/reset-password/';

            // Search for User's in Collection
            user = await this._userRepository.findByEmail(userEmail);

            if (!user) {
                throw <IBasicError>{
                    code: 'EMAIL_NOT_FOUND',
                    field: 'email',
                    message: 'Email not found.'
                };
            }

            // Set recovery password token (with expiration date)
            user.local.passwordReset = {
                token: token,
                expired: moment().add(2, 'hours').toDate()
            };

            updatedUser = await this._userRepository.update(user._id, user);

            if (!updatedUser) {
                throw <IBasicError>{
                    code: 'PASSWORD_TOKEN',
                    field: 'token',
                    message: 'There was with the password token.'
                };
            }

            // Set replacers will be used in the email template
            replacers = {
                name: user.firstName ? user.firstName : 'there',
                url: resetPasswordUrl + token
            }

            await this._emailServices.sendMail(
                '/../emails/forgot-password.html', // template
                userEmail.toString(), // to
                'Here comes the help!', //subject
                replacers //replacers
            );

            return res.status(200).json('Email Sent');

        } catch (error) {
            return res.status(400).json(error);
        }
    }

    public async findByPasswordToken(req: Request, res: Response): Promise<Response> {
        try {
            let token: String = req.params.token;
            let user = await UserController._userRepository.findByValidPasswordToken(token);
            return res.status(200).json(user);
        } catch (err) {
            return res.status(400).json(err);
        }
    }

    public async validatePasswordToken(req: Request, res: Response): Promise<Response> {
        try {
            let token: String = req.params.token;
            let user = await UserController._userRepository.findByValidPasswordToken(token);

            if (!user) {
                throw <IBasicError>{
                    code: 'INVALID_TOKEN',
                    field: 'token',
                    message: 'Invalid or expired token. Try reseting your password again.'
                };
            }

            return res.status(200).json(user._id);
        } catch (error) {
            return res.status(400).json(error);
        }
    }

    public async resetPassword(req: Request, res: Response): Promise<Response> {
        try {
            let token: String = req.params.token;
            let user = await UserController._userRepository.findByValidPasswordToken(token);

            if (!user) {
                throw <IBasicError>{
                    code: 'INVALID_TOKEN',
                    field: 'token',
                    message: 'Invalid or expired token. Try reseting your password again.'
                };
            }

            if (req.body.password !== req.body.passwordValidation) {
                throw <IBasicError>{
                    code: 'INVALID_PASSWORD',
                    field: 'password',
                    message: 'Passwords does not match.'
                };
            }

            user.local.password = UserController._userRepository.generatePasswordHash(req.body.password);

            await UserController._userRepository.update(user._id.toString(), user);

            return res.status(200).json(user._id);
        } catch (error) {
            return res.status(400).json(error);
        }

    }
}
