import { ITranslation } from './translation';

export interface IUnitMeasure {
    code: String,
    name: ITranslation,
    value: Number
}