import { Document } from 'mongoose';
import { ITournament } from './tournament';

export interface ITournamentDocument extends ITournament, Document{
    //
}
