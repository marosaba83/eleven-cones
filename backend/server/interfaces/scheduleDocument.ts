import { Document } from 'mongoose';
import { ISchedule } from './schedule';

export interface IScheduleDocument extends ISchedule, Document{
    //
}
