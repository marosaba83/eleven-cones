import { IFoot } from './foot';
import { IUnitMeasure } from './unitMeasure';
import { ISocialLogin } from './SocialLogin';

export interface IUser {
    firstName?: String,
    lastName?: String,
    email: String,
    picture?: String,
    pictureUrl?: String,
    birthdate?: Date,
    strongFoot?: IFoot,
    backNumber?: Number,
    height?: IUnitMeasure,
    weight?: IUnitMeasure,
    local?: {
        password?: String,
        passwordReset?: {
            token: String,
            expired: Date
        }
    },
    google?: ISocialLogin,
    facebook?: ISocialLogin,
    // twitter?: ISocialLogin,
    // roles: IRole[],
    // manageesTo: ITeam[],
    // playsOn: [
    //     {
    //         teamId: ITeam,
    //         shirtNumber: Number,ls
    //         position: IPosition
    //     }
    // ],
    isActive: Boolean,
    inactiveDate?: Date
}
