export interface ISocialLogin {
    id?: String,
    token?: String,
    name?: String,
    username?: String
}