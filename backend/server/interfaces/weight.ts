import { ITranslation } from './translation';

export interface IWeight {
    code: String,
    name: ITranslation,
    value?: Number,
}
