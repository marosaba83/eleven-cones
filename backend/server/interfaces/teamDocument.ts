import { Document } from 'mongoose';
import { ITeam } from './team';

export interface ITeamDocument extends ITeam, Document{
    //
}
