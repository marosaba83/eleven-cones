import { ITranslation } from './translation';

export interface IHeight {
    code: String,
    name: ITranslation,
    value?: Number,
}
