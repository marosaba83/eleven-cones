import { Document } from 'mongoose';
import { IUser } from './user';

export interface IUserDocument extends IUser, Document{
    //
}
