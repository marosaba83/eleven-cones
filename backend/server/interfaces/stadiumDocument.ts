import { Document } from 'mongoose';
import { IStadium } from './stadium';

export interface IStadiumDocument extends IStadium, Document{
    //
}
