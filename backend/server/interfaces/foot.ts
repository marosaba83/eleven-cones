import { ITranslation } from './translation';

export interface IFoot {
    code: String,
    name: ITranslation
}
